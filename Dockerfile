FROM node:20.8.0-alpine as build
RUN apk update
RUN apk upgrade
RUN apk add gettext

WORKDIR /app

COPY . .

RUN npm install --legacy-peer-deps
RUN npm run build -env=production --configuration=production

FROM nginx:alpine

COPY /external/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist/git-sync-frontend/ /usr/share/nginx/html

EXPOSE 80
