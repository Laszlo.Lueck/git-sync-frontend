import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from "./main/main/main.component";
import {HistoryComponent} from "./main/history/history.component";

const routes: Routes = [
  //{path: 'home', loadChildren: () => import('./main/main.module').then(m => m.MainModule) },
  {path: 'home', component: MainComponent},
  {path: 'history', component: HistoryComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
