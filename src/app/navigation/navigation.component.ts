import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../services/CommonDataService";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy {
  subscription!: Subscription;
  title!: string;

  constructor(private commonDataService: CommonDataService) { }

  ngOnDestroy(): void {
    if(this.commonDataService)
    {
      this.commonDataService.clearData();
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.subscription = this.commonDataService.getData().subscribe(d => this.title = d);
  }

}
