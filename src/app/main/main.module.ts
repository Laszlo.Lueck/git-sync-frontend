import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { HistoryComponent } from './history/history.component';
import { MatCardModule } from "@angular/material/card";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import { PushEntryEditDialogComponent } from './main/components/push-entry-edit-dialog/push-entry-edit-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ConfirmDeleteDialogComponent } from './main/components/confirm-delete-dialog/confirm-delete-dialog.component';
import {MatPaginatorIntl, MatPaginatorModule} from "@angular/material/paginator";
import {PaginatorLocalization} from "./history/PaginatorLocalization";
import {MatSelectModule} from "@angular/material/select";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import { PushEntryNewDialogComponent } from './main/components/push-entry-new-dialog/push-entry-new-dialog.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import { ActionErrorDialogComponent } from './main/components/action-error-dialog/action-error-dialog.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { HistoryEntryComponent } from './history/components/history-entry/history-entry.component';
import { MainViewItemComponent } from './main/components/main-view-item/main-view-item.component';

@NgModule({
  declarations: [
    MainComponent,
    HistoryComponent,
    PushEntryEditDialogComponent,
    ConfirmDeleteDialogComponent,
    PushEntryNewDialogComponent,
    ActionErrorDialogComponent,
    HistoryEntryComponent,
    MainViewItemComponent
  ],
    imports: [
        CommonModule,
        MatCardModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatPaginatorModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        MatProgressBarModule
    ],
  providers: [
    {provide: MatPaginatorIntl, useValue: PaginatorLocalization()}
  ]
})
export class MainModule { }
