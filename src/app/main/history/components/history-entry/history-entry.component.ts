import {Component, Input} from '@angular/core';
import {HistoryEntity} from "../../services/pushEntryHistory";

@Component({
  selector: 'app-history-entry',
  templateUrl: './history-entry.component.html',
  styleUrls: ['./history-entry.component.css']
})
export class HistoryEntryComponent {
  @Input() historyEntity!: HistoryEntity;



}
