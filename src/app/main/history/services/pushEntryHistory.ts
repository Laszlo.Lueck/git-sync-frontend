export interface PushEntryHistory{
  pageCount: number;
  docCount: number;
  dataList: HistoryEntity[];
}



export interface HistoryEntity {
  projectId: string;
  name: string;
  _id: string;
  directoryName: string;
  createDate: Date;
  lastChange: Date;
  historyDate: Date;
  lastDockerTag: string;
}


