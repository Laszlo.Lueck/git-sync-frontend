import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable, take} from "rxjs";
import {Either, makeRight} from "../../../generic/either";
import {BaseError} from "../../../generic/baseError";
import {PushEntryHistory} from "./pushEntryHistory";
import {environment} from "../../../../environments/environment";
import {getErrorHandler} from "../../../generic/helper";

@Injectable({
  providedIn: 'root'
})
export class GetHistoryDataFilterByProjectIdService {

  getHistoryData(page: number, pageSize: number, projectId: string): Observable<Either<BaseError, PushEntryHistory>> {
    return this
      .httpClient
      .get<PushEntryHistory>(`${environment.apiUrl}api/history/list/projectId/${projectId}/${page}/${pageSize}`)
      .pipe(
        take(1),
        map(result => makeRight(result)),
        catchError(getErrorHandler<PushEntryHistory>('getHistoryData'))
      );
  }


  constructor(private httpClient: HttpClient) { }
}
