import { TestBed } from '@angular/core/testing';

import { GetHistoryDataService } from './get-history-data.service';
import {HttpClient} from "@angular/common/http";

describe('GetHistoryDataService', () => {
  let service: GetHistoryDataService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new GetHistoryDataService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
