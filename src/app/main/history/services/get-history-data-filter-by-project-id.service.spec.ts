import { TestBed } from '@angular/core/testing';

import { GetHistoryDataFilterByProjectIdService } from './get-history-data-filter-by-project-id.service';
import {HttpClient} from "@angular/common/http";

describe('GetHistoryDataFilterByProjectIdService', () => {
  let service: GetHistoryDataFilterByProjectIdService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new GetHistoryDataFilterByProjectIdService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
