import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {PushEntryHistory} from "./pushEntryHistory";
import {environment} from "../../../../environments/environment";
import {catchError, map, Observable, take} from "rxjs";
import {Either, makeRight} from "../../../generic/either";
import {getErrorHandler} from "../../../generic/helper";
import {BaseError} from "../../../generic/baseError";

@Injectable({
  providedIn: 'root'
})
export class GetHistoryDataService {

  getHistoryData(page: number, pageSize: number): Observable<Either<BaseError, PushEntryHistory>> {
    return this
      .httpClient
      .get<PushEntryHistory>(`${environment.apiUrl}api/history/list/${page}/${pageSize}`)
      .pipe(
        take(1),
        map(result => makeRight(result)),
        catchError(getErrorHandler<PushEntryHistory>('getHistoryData'))
      );
  }


  constructor(private httpClient: HttpClient) {
  }
}
