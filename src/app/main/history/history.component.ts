import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../../services/CommonDataService";
import {PageEvent} from "@angular/material/paginator";
import {GetHistoryDataService} from "./services/get-history-data.service";
import {HistoryEntity, PushEntryHistory} from "./services/pushEntryHistory";
import {BaseError} from "../../generic/baseError";
import {Either, match} from "../../generic/either";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Subscription} from "rxjs";
import {CurrentDataLoaderService} from "../main/services/current-data-loader.service";
import {PushEntry} from "../main/services/pushEntry";
import {GetHistoryDataFilterByProjectIdService} from "./services/get-history-data-filter-by-project-id.service";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit, OnDestroy {

  length = 50;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];

  hidePageSize = false;
  showPageSizeOptions = true;
  showFirstLastButtons = true;
  disabled = false;

  pageEvent!: PageEvent;

  selected = 'None';

  private subscription!: Subscription;
  public historyPushEntries!: HistoryEntity[];
  public currentPushEntries!: PushEntry[];

  handlePageEvent(e: PageEvent) {
    console.log("Event is: ");
    console.log(e);

    this.pageEvent = e;
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.loadHistoryData();
  }

  handleFilterEvent(): void {
    this.pageIndex = 0;
    this.loadHistoryData();
  }

  constructor(
    private commonDataService: CommonDataService,
    private getHistoryDataService: GetHistoryDataService,
    private getFilteredHistoryDataService: GetHistoryDataFilterByProjectIdService,
    private _snackBar: MatSnackBar,
    private currentDataLoader: CurrentDataLoaderService
  ) {
  }

  ngOnInit(): void {
    this.commonDataService.sendData('Verlauf');
    this.loadDistinctedPushEntries();
    this.loadHistoryData();
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  loadDistinctedPushEntries(): void {
    this
      .currentDataLoader
      .getPushEntries()
      .subscribe((data: Either<BaseError, PushEntry[]>) => {
        match(
          data,
          left => this._snackBar.open(left.errorMessage + " " + left.errorCode, " Fehler!", {
            duration: 10000,
            horizontalPosition: "center",
            verticalPosition: "top"
          }),
          right => this.currentPushEntries = right
        )
      });
  }

  loadHistoryData() {
    console.log("Filter ist: " + this.selected);
    if (this.selected === 'None') {
      this
        .getHistoryDataService
        .getHistoryData(this.pageIndex + 1, this.pageSize)
        .subscribe((data: Either<BaseError, PushEntryHistory>) => {
          match(
            data,
            left => this._snackBar.open(left.errorMessage + " " + left.errorCode, " Fehler!", {
              duration: 10000,
              horizontalPosition: "center",
              verticalPosition: "top"
            }),
            right => {
              this.historyPushEntries = right.dataList;
              this.length = right.docCount;
            })
        })
    } else {
      this
        .getFilteredHistoryDataService
        .getHistoryData(this.pageIndex + 1, this.pageSize, this.selected)
        .subscribe((data: Either<BaseError, PushEntryHistory>) => {
          match(
            data,
            left => this._snackBar.open(left.errorMessage + " " + left.errorCode, " Fehler!", {
              duration: 10000,
              horizontalPosition: "center",
              verticalPosition: "top"
            }),
            right => {
              this.historyPushEntries = right.dataList;
              this.length = right.docCount;
            })
        })
    }
  }
}
