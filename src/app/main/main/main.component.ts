import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonDataService} from "../../services/CommonDataService";
import {CurrentDataLoaderService} from "./services/current-data-loader.service";
import {Subscription} from "rxjs";
import {PushEntry} from "./services/pushEntry";
import {Either, match} from "../../generic/either";
import {BaseError} from "../../generic/baseError";
import {PushEntryEditData} from "./components/push-entry-edit-dialog/PushEntryEditData";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {PushEntryEditDialogComponent} from "./components/push-entry-edit-dialog/push-entry-edit-dialog.component";
import {SaveCurrentDataService} from "./services/save-current-data.service";
import {SaveNewDataServiceService} from "./services/save-new-data-service.service";
import {ConfirmDeleteDialogComponent} from "./components/confirm-delete-dialog/confirm-delete-dialog.component";
import {DeleteDialogData} from "./components/confirm-delete-dialog/DeleteDialogData";
import {DeletePushEntryService} from "./services/delete-push-entry.service";
import {PushEntryNewDialogComponent} from "./components/push-entry-new-dialog/push-entry-new-dialog.component";
import {ReadOnePushEntryDetailService} from "./services/read-one-push-entry-detail.service";
import {NewCredential, PushEntryNewData} from "./components/push-entry-new-dialog/PushEntryNewData";
import {DeletePushEntryResult} from "./services/deletePushEntryResult";
import {ActionErrorDialogData, DialogType} from "./components/action-error-dialog/action-error-dialog-data";
import {ActionErrorDialogComponent} from "./components/action-error-dialog/action-error-dialog.component";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
  private subscription!: Subscription;
  public pushEntries!: PushEntry[];

  constructor(
    private commonDataService: CommonDataService,
    private currentDataLoaderService: CurrentDataLoaderService,
    private readOnePushEntryService: ReadOnePushEntryDetailService,
    private saveCurrentDataService: SaveCurrentDataService,
    private saveNewDataService: SaveNewDataServiceService,
    private deletePushEntryService: DeletePushEntryService,
    public dialog: MatDialog,
  ) {
  }


  ngOnInit(): void {
    this.commonDataService.sendData('Startseite');
    this.loadData();
  }

  handleEditEvent(eventHandler: PushEntry){
    this.openEditDialog(eventHandler.projectId);
  }

  handleDeleteEvent(eventHandler: PushEntry) {
    this.deletePushEntry(eventHandler.projectId);
  }

  loadData(): void {
    this.subscription = this
      .currentDataLoaderService
      .getPushEntries()
      .subscribe((data: Either<BaseError, PushEntry[]>) => {
        match(
          data,
          left => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
          },
          right => this.pushEntries = right
        )
      });
  }

  deletePushEntry(projectId: string): void {
    const deleteDialogData: DeleteDialogData = {
      shouldDelete: false,
      projectId: projectId
    };

    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      exitAnimationDuration: '0.5s',
      enterAnimationDuration: '0.5s',
      data: deleteDialogData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.shouldDelete)
        this.deleteDataAndRefreshList(result);
    });

  }

  deleteDataAndRefreshList(deleteDialogData: DeleteDialogData): void {
    this
      .deletePushEntryService
      .deletePushEntry(deleteDialogData.projectId)
      .subscribe((either: Either<BaseError, DeletePushEntryResult>) => {
        match(
          either,
          left => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
          },
          right => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 3,
              headline: 'Erfolg!',
              body: 'Die Daten wurden erfolgreich gelöscht',
              code: '',
              dialogType: DialogType.Success
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
            this.loadData();
          });
      })
  }

  openNewDialog(): void {

    const newCredential: NewCredential = {
      branchName: '',
      gitDirectoryName: '',
      gitConfigEmail: '',
      gitConfigUser: '',
      gitCredentialToken: '',
      gitCredentialTokenName: '',
      gitProjectUrl: '',
      gitRepoRemote: '',
      gitRepoPushRefSpec: '',
      templateFileName: ''
    };
    const transport: PushEntryNewData = {name: '', active: true, credential: newCredential};
    const dialogRef: MatDialogRef<PushEntryNewDialogComponent> = this.dialog.open(PushEntryNewDialogComponent, {
      enterAnimationDuration: '0.5s',
      width: '640px',
      data: transport
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.saveNewDataAndRefreshList(result);
    });

  }


  openEditDialog(projectId: string): void {
    this
      .readOnePushEntryService
      .getOnePushEntry(projectId)
      .subscribe((either: Either<BaseError, PushEntryEditData>): void => {
        match(either,
          left => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
          },
          right => {
            const dialogRef = this.dialog.open(PushEntryEditDialogComponent, {
              width: '640px',
              enterAnimationDuration: '0.5s',
              data: right
            });

            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log("we received a save event from dialog for " + JSON.stringify(result));
                this.saveDataAndRefreshList(result);
              }
            });
          });
      });
  }

  saveNewDataAndRefreshList(dialogData: PushEntryNewData): void {
    this
      .saveNewDataService
      .saveNewPushEntry(dialogData)
      .subscribe((either: Either<BaseError, string>): void => {
        match(either,
          left => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
          },
          _ => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 3,
              headline: 'Erfolg!',
              body: 'Der Datensatz wurde erfolgreich angelegt',
              code: '',
              dialogType: DialogType.Success
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
            this.loadData();
          })

      });
  }

  saveDataAndRefreshList(dialogData: PushEntryEditData): void {
    this
      .saveCurrentDataService
      .saveEditedEntry(dialogData)
      .subscribe((either: Either<BaseError, string>) => {
        match(either,
          left => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 5,
              headline: 'Ein Fehler ist aufgetreten',
              body: `Funktion: ${left.operation}`,
              code: JSON.stringify(left.errorMessage, null, 3),
              dialogType: DialogType.Error
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
          },
          _ => {
            const data: ActionErrorDialogData = {
              visibleForSeconds: 3,
              headline: 'Erfolg!',
              body: 'Der Datensatz wurde erfolgreich gespeichert',
              code: '',
              dialogType: DialogType.Success
            };
            this.dialog.open(ActionErrorDialogComponent, {
              width: '100%',
              enterAnimationDuration: '0.5s',
              exitAnimationDuration: '0.5s',
              position: {top: '0px'},
              data: data
            });
            this.loadData();
          })
      });
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }


  protected readonly Event = Event;
}
