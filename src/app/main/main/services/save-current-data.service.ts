import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {catchError, map, Observable, take} from "rxjs";
import {getErrorHandler} from "../../../generic/helper";
import {Either, makeRight} from "../../../generic/either";
import {BaseError} from "../../../generic/baseError";
import {PushEntryEditData} from "../components/push-entry-edit-dialog/PushEntryEditData";

@Injectable({
  providedIn: 'root'
})
export class SaveCurrentDataService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  saveEditedEntry(pushEntryEditData: PushEntryEditData): Observable<Either<BaseError, string>> {

    return this
      .httpClient
      .post<string>(`${environment.apiUrl}api/update/pushEntry`, pushEntryEditData, this.httpOptions)
      .pipe(
        take(1),
        map(result => makeRight(result)),
        catchError(getErrorHandler<string>('saveEditedEntry'))
      );
  }

  constructor(private httpClient: HttpClient) {
  }
}
