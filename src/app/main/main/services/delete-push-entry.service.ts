import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable, take} from "rxjs";
import {Either, makeRight} from "../../../generic/either";
import {BaseError} from "../../../generic/baseError";
import {environment} from "../../../../environments/environment";
import {getErrorHandler} from "../../../generic/helper";
import {DeletePushEntryResult} from "./deletePushEntryResult";

@Injectable({
  providedIn: 'root'
})
export class DeletePushEntryService {

  public deletePushEntry(projectId: string): Observable<Either<BaseError, DeletePushEntryResult>> {
    return this
      .httpClient
      .delete<DeletePushEntryResult>(`${environment.apiUrl}api/delete/${projectId}`)
      .pipe(
        take(1),
        map(result => makeRight(result)),
        catchError(getErrorHandler<DeletePushEntryResult>('deletePushEntry'))
      );
  }

  constructor(private httpClient: HttpClient) {
  }
}
