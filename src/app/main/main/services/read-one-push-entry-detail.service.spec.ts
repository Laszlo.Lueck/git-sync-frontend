import { TestBed } from '@angular/core/testing';

import { ReadOnePushEntryDetailService } from './read-one-push-entry-detail.service';
import { HttpClient } from "@angular/common/http";

describe('ReadOnePushEntryDetailService', () => {
  let service: ReadOnePushEntryDetailService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new ReadOnePushEntryDetailService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
