export interface DeletePushEntryResult {
  projectId: string;
  pushEntrySuccess: boolean;
  credentialSuccess: boolean;
  deletedHistories: number;
  hasError: boolean;
  errorMessage: string;
}
