import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {catchError, map, Observable, take} from "rxjs";
import {Either, makeRight} from "../../../generic/either";
import {BaseError} from "../../../generic/baseError";
import {PushEntry} from "./pushEntry";
import {getErrorHandler} from "../../../generic/helper";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CurrentDataLoaderService {

  getPushEntries(): Observable<Either<BaseError, PushEntry[]>> {
    return this
      .httpClient
      .get<PushEntry[]>(`${environment.apiUrl}api/list`)
      .pipe(
        take(1),
        map(result => makeRight(result)),
        catchError(getErrorHandler<PushEntry[]>('getPushEntries'))
      );
  }

  constructor(private httpClient: HttpClient) {
  }
}
