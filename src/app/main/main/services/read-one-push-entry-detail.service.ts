import {Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {catchError, map, Observable, take} from "rxjs";
import {Either, makeRight} from "../../../generic/either";
import {BaseError} from "../../../generic/baseError";
import {PushEntryEditData} from "../components/push-entry-edit-dialog/PushEntryEditData";
import {environment} from "../../../../environments/environment";
import {getErrorHandler} from "../../../generic/helper";

@Injectable({
  providedIn: 'root'
})
export class ReadOnePushEntryDetailService {

  getOnePushEntry(projectId: string): Observable<Either<BaseError, PushEntryEditData>> {
    return this
      .httpClient
      .get<PushEntryEditData>(`${environment.apiUrl}api/get/${projectId}`)
      .pipe(
        take(1),
        map(result => makeRight(result)),
        catchError(getErrorHandler<PushEntryEditData>('getOnePushEntry'))
      );
  }

  constructor(private httpClient: HttpClient) {
  }
}
