export interface NewOrUpdateEntry {
  name: string;
  directoryName: string;
}
