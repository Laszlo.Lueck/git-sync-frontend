import { TestBed } from '@angular/core/testing';

import { CurrentDataLoaderService } from './current-data-loader.service';
import { HttpClient } from "@angular/common/http";

describe('CurrentDataLoaderService', () => {
  let service: CurrentDataLoaderService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;


  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new CurrentDataLoaderService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
