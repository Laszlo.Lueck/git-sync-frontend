export interface PushEntry {
  projectId: string;
  name: string;
  createDate: Date;
  lastChange: Date;
  lastDockerTag: string;
  active: boolean;
}
