import { TestBed } from '@angular/core/testing';

import { SaveNewDataServiceService } from './save-new-data-service.service';
import {HttpClient} from "@angular/common/http";

describe('SaveNewDataServiceService', () => {
  let service: SaveNewDataServiceService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['put']);
    service = new SaveNewDataServiceService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
