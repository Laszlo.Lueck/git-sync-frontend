import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, catchError, map, take} from 'rxjs';
import {BaseError} from 'src/app/generic/baseError';
import {Either, makeRight} from 'src/app/generic/either';
import {NewOrUpdateEntry} from './newOrUpdateEntry';
import {getErrorHandler} from 'src/app/generic/helper';
import {environment} from 'src/environments/environment';
import {PushEntryNewData} from "../components/push-entry-new-dialog/PushEntryNewData";

@Injectable({
  providedIn: 'root'
})
export class SaveNewDataServiceService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  saveNewPushEntry(data: PushEntryNewData): Observable<Either<BaseError, string>> {
    return this
      .httpClient
      .put<string>(`${environment.apiUrl}api/create/pushentry`, data, this.httpOptions)
      .pipe(
        take(1),
        map(result => makeRight(result)),
        catchError(getErrorHandler<string>('saveNewPushEntry'))
      );
  }

  constructor(private httpClient: HttpClient) {
  }
}
