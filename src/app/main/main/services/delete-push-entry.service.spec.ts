import { TestBed } from '@angular/core/testing';

import { DeletePushEntryService } from './delete-push-entry.service';
import { HttpClient } from "@angular/common/http";
import {H} from "@angular/cdk/keycodes";

describe('DeletePushEntryService', () => {
  let service: DeletePushEntryService;
  let httpClientSpy:  jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['delete']);
    service = new DeletePushEntryService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
