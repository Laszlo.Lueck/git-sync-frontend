import { TestBed } from '@angular/core/testing';

import { SaveCurrentDataService } from './save-current-data.service';
import { HttpClient } from "@angular/common/http";

describe('SaveCurrentDataService', () => {
  let service: SaveCurrentDataService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    service = new SaveCurrentDataService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
