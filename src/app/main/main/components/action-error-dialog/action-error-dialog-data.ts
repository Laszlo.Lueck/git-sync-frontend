export interface ActionErrorDialogData {
  headline: string;
  body: string;
  code: string;
  visibleForSeconds: number;
  dialogType: DialogType
}

export enum DialogType {
  Success = 0,
  Error = 1
}
