import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PushEntry} from "../../services/pushEntry";

@Component({
  selector: 'app-main-view-item',
  templateUrl: './main-view-item.component.html',
  styleUrls: ['./main-view-item.component.css']
})
export class MainViewItemComponent {
  @Input() pushEntry!: PushEntry;
  @Output() editEntry: EventEmitter<PushEntry> = new EventEmitter<PushEntry>();
  @Output() deleteEntry: EventEmitter<PushEntry> = new EventEmitter<PushEntry>();

  emitOpenEditDialog(pushEntry: PushEntry) {
    this.editEntry.emit(pushEntry);
  }

  emitOpenDeleteDialog(pushEntry: PushEntry) {
    this.deleteEntry.emit(pushEntry);
  }

}
