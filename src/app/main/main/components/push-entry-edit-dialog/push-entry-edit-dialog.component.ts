import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PushEntryEditData} from "./PushEntryEditData";
import {NgModel} from "@angular/forms";

@Component({
  selector: 'app-push-entry-edit-dialog',
  templateUrl: './push-entry-edit-dialog.component.html',
  styleUrls: ['./push-entry-edit-dialog.component.css']
})
export class PushEntryEditDialogComponent {

  constructor(public dialogRef: MatDialogRef<PushEntryEditDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: PushEntryEditData) {

  }

  errorMessage(fieldName: string): string {
    return `Das Feld ${fieldName} ist ein Pflichtfeld`;
  }


  checkValidation(model: NgModel): boolean {
    return (model && model.invalid) ?? false;
  }

  checkElements(elements: NgModel[]): boolean {
    return elements.filter(element => element.invalid).length > 0;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
