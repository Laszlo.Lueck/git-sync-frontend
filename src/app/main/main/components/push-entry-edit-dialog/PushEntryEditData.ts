export interface PushEntryEditData {
  projectId: string;
  name: string;
  active: boolean;
  credential: EditCredential;
}

export interface EditCredential {
  branchName: string;
  gitConfigUser: string;
  gitConfigEmail: string;
  gitCredentialTokenName: string;
  gitCredentialToken: string;
  gitRepoRemote: string;
  gitRepoPushRefSpec: string;
  templateFileName: string;
  createDate: Date;
  lastChangeDate: Date;
  gitProjectUrl: string;
  gitDirectoryName: string;
}
