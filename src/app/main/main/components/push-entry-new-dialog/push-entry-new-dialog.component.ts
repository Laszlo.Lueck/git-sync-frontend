import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PushEntryNewData} from "./PushEntryNewData";
import {FormControl, FormGroup, NgModel, Validators} from "@angular/forms";

@Component({
  selector: 'app-push-entry-new-dialog',
  templateUrl: './push-entry-new-dialog.component.html',
  styleUrls: ['./push-entry-new-dialog.component.css']
})
export class PushEntryNewDialogComponent {
  constructor(public dialogRef: MatDialogRef<PushEntryNewDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: PushEntryNewData) {

  }


  errorMessage(fieldName: string): string {
    return `Das Feld ${fieldName} ist ein Pflichtfeld`;
  }


  checkValidation(model: NgModel): boolean {
    return (model && model.invalid)??false;
  }

  checkElements(elements: NgModel[]): boolean {
    return elements.filter(element => element.invalid).length > 0;
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
}
