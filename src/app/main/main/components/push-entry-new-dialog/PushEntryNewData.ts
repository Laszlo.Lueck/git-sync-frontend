﻿export interface PushEntryNewData {
  active: boolean;
  name: string;
  credential: NewCredential;
}


export interface NewCredential {
  branchName: string;
  gitConfigUser: string;
  gitConfigEmail: string;
  gitCredentialTokenName: string;
  gitCredentialToken: string;
  gitRepoRemote: string;
  gitRepoPushRefSpec: string;
  templateFileName: string;
  gitProjectUrl: string;
  gitDirectoryName: string;
}
