import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DeleteDialogData} from "./DeleteDialogData";

@Component({
  selector: 'app-confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.component.html',
  styleUrls: ['./confirm-delete-dialog.component.css']
})
export class ConfirmDeleteDialogComponent {

  constructor(public dialogRef: MatDialogRef<ConfirmDeleteDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DeleteDialogData) {
  }

  onOkClick(): void{
    this.data.shouldDelete = true;
    this.dialogRef.close(this.data);
  }

  onNoClick(): void {
    this.data.shouldDelete = false;
    this.dialogRef.close(this.data);
  }

}
