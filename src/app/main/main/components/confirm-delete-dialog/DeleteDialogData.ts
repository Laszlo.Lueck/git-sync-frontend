﻿export interface DeleteDialogData{
  projectId: string;
  shouldDelete: boolean;
}
