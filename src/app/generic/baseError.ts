export interface BaseError {
  errorMessage: string;
  errorCode: number;
  operation: string;
}
